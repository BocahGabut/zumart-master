@extends(MarketplaceHelper::viewPath('dashboard.layouts.master'))
@section('content')
    <div class="container page-content" style="background: none; max-width: none">
        <div class="table-wrapper">
            @if ($table->isHasFilter())
                <div class="table-configuration-wrap" @if (request()->has('filter_table_id')) style="display: block;" @endif>
                    <span class="configuration-close-btn btn-show-table-options"><i class="fa fa-times"></i></span>
                    {!! $table->renderFilter() !!}
                </div>
            @endif
            <div class="portlet light bordered portlet-no-padding">
                <div class="portlet-title">
                    <div class="caption">
                        <div class="wrapper-action">
                            @if ($actions)
                                <div class="btn-group">
                                    <a class="btn btn-secondary dropdown-toggle" href="#"
                                        data-bs-toggle="dropdown">{{ trans('core/table::table.bulk_actions') }}
                                    </a>
                                    <ul class="dropdown-menu">
                                        @foreach ($actions as $action)
                                            <li>
                                                {!! $action !!}
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if ($table->isHasFilter())
                                <button
                                    class="btn btn-primary btn-show-table-options">{{ trans('core/table::table.filters') }}</button>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive @if ($actions) table-has-actions @endif @if ($table->isHasFilter()) table-has-filter @endif"
                        style="overflow-x: inherit">
                    @section('main-table')
                        {!! $dataTable->table(['data-table' => 'collapse-table'], false) !!}
                        {{--  {!! $dataTable->table(compact('id', 'class'), false) !!}  --}}
                    @show
                </div>
            </div>
        </div>
    </div>
</div>
@stop
@push('scripts')
{!! $dataTable->scripts() !!}
<script>
    $(document).ready(function() {
        setTimeout(() => {
            var table = $('table[data-table="collapse-table"]').DataTable();
            $(document).on('click', 'tr.collapsed', function() {
                var tr = $(event.target).closest('tr');
                var row = table.row(tr);
                var group = tr.data('group');

                {{--  if (row.child.isShown()) {
                    row.child.hide();
                    tr.removeClass('shown');
                    tr.toggleClass('collapsed', true);
                    tr.toggleClass('expanded', false);
                } else {
                    row.child(formatChildData(row.data())).show();
                    tr.addClass('shown');
                    tr.toggleClass('collapsed', false);
                    tr.toggleClass('expanded', true);
                }  --}}
            })
        }, 450)
    });
</script>
{{--  <script>
    $('#botble-marketplace-tables-etalase-table_processing tbody').on('click', 'td.details-control', function() {
        var tr = $(this).closest('tr');
        var row = table.row(tr);
        var group = tr.data('group');

        if (row.child.isShown()) {
            // Baris child sudah terbuka, tutup
            row.child.hide();
            tr.removeClass('shown');
            tr.toggleClass('collapsed', true);
            tr.toggleClass('expanded', false);
        } else {
            // Baris child belum terbuka, buka
            row.child(formatChildRow(row.data())).show();
            tr.addClass('shown');
            tr.toggleClass('collapsed', false);
            tr.toggleClass('expanded', true);
        }
    });

    // Fungsi untuk memformat konten child row
    function formatChildRow(data) {
        // Ubah sesuai dengan konten yang ingin ditampilkan pada child row
        var childContent = '<table>' +
            '<tr>' +
            '<td>' + data.name + '</td>' +
            '<td>' + data.price + '</td>' +
            // tambahkan kolom lain yang Anda perlukan
            '</tr>' +
            '</table>';
        return childContent;
    }
</script>  --}}
{{--  {!! $dataTable->scripts() !!}  --}}
@endpush
