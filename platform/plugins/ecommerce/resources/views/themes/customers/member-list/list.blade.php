@extends(EcommerceHelper::viewPath('customers.master'))

@section('content')

    <h2 class="customer-page-title">{{ __('Member Downline') }}</h2>

    <div class="customer-list-order">
    	@if ($orders->count())
        <table class="table  table-hover">
            <thead>
            <tr class="success">
                <th>{{ __('Id Customer') }}</th>
                <th>{{ __('Nama') }}</th>
                <th>{{ __('Status') }}</th>                
                
            </tr></thead>
            <tbody>
                @foreach ($orders as $order)
                    <tr>
                        <td>#{{ config('plugins.ecommerce.order.order_code_prefix') }}{{ $order->id }}</td>
                        <td>{{ $order->created_at->format('h:m d/m/Y') }}</td>                        
                        <td>{{ $order->status->label() }}</td>                        
                    </tr>
                @endforeach
            </tbody>
        </table>

        @else
        	<div class="alert alert-info">
            	<span>{{ __('You do not have any products to review yet. Just shopping!') }}</span>
            </div>
        @endif

    </div>
@endsection
