<?php

namespace Theme\Farmart\Http\Controllers;

use Botble\Payment\Models\Midtrans;
use Botble\Payment\Models\Payment;
use Botble\Theme\Http\Controllers\PublicController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PaymentController extends PublicController
{
    public function getIndex(){
        return 'aaa';
    }

    public function postStore(Request $request) {
        (object)$request;

        if ($request->status_code === '200') {
            // $GET_PAYMENT_DATA = DB::select("SELECT * FROM payments WHERE charge_id = '$request->order_id' AND status = 'pending'");
            $GET_PAYMENT_DATA = Payment::where([['charge_id',$request->order_id]])->get();
            if(count($GET_PAYMENT_DATA) > 0) {
                foreach($GET_PAYMENT_DATA as $GPD){
                    $GET_PAYMENT_ID = Payment::where('id',$GPD->id)->first();
                    $GET_PAYMENT_ID->status = 'completed';
                    $GET_PAYMENT_ID->save();
                    // return $GET_PAYMENT_ID;
                }
            }else{
                return response()->json(['error' => 'Internal Server Error'], 500);
            }
        }else if ($request->status_code === '201'){
            $MIDTRANS = new Midtrans;
            $MIDTRANS->order_id = $request->order_id;
            $MIDTRANS->metadata = json_encode($request->input());
            $MIDTRANS->save();

            // return response()->json(['error' => 'Internal Server Error'], 500);
            // return $request->va_numbers[0]['bank'];
        }
    }

}
