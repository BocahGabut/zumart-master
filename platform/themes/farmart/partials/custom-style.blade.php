<style>
    .css-kqc12l {
        max-width: 1080px;
        margin: 0px 0px 0px 24px;
        padding: 16px;
        border-radius: 6px;
        border: 1px solid rgb(224, 224, 224);
    }

    .css-xr2eo5 {
        display: flex;
        -webkit-box-pack: justify;
        justify-content: space-between;
        -webkit-box-align: baseline;
        align-items: baseline;
        margin-bottom: 16px;
        min-width: 729px;
    }

    .css-15yrpiz {
        color: rgba(49, 53, 59, 0.68);
        font-size: 14px;
        font-weight: bold;
        line-height: 16px;
        margin: 0px;
    }

    .css-s4jn04-unf-card {
        display: block;
        position: relative;
        margin: 16px 0px;
        padding: 16px;
        border-radius: 8px;
        border-color: var(--NN200, #D6DFEB);
        box-shadow: rgba(141, 150, 170, 0.4) 0px 1px 4px;
        background-color: var(--NN0, #FFFFFF);
        cursor: default;
    }

    .css-s4jn04-unf-card {
        display: block;
        position: relative;
        margin: 16px 0px;
        padding: 16px;
        border-radius: 8px;
        border-color: var(--NN200, #D6DFEB);
        box-shadow: rgba(141, 150, 170, 0.4) 0px 1px 4px;
        background-color: var(--NN0, #FFFFFF);
        cursor: default;
    }

    .css-69i1ev {
        display: flex;
        -webkit-box-pack: justify;
        justify-content: space-between;
        -webkit-box-align: center;
        align-items: center;
    }

    .css-1k6yql2 {
        display: flex;
        -webkit-box-pack: start;
        justify-content: flex-start;
        -webkit-box-align: center;
        align-items: center;
    }

    .css-1r8lly0 {
        font-weight: bold;
        font-size: 14px;
        line-height: 20px;
        color: var(--N700, rgba(49, 53, 59, 0.96));
        margin: 0px 8px;
    }

    .css-8lnd9g {
        color: rgba(49, 53, 59, 0.68);
        font-size: 12px;
        font-weight: unset;
        line-height: 16px;
        margin: 0.25rem 0px;
    }

    .css-5gojna {
        color: rgba(49, 53, 59, 0.68);
        font-size: 12px;
        font-weight: unset;
        line-height: 16px;
        margin: 0px 4px 0px 0px;
    }

    .css-1qmp87g {
        color: var(--Y400, #FF8B00);
        font-size: 12px;
        font-weight: bold;
        line-height: 16px;
        margin: 0px 0px 0px 4px;
    }

    .css-1hvhwyw {
        display: flex;
        -webkit-box-pack: justify;
        justify-content: space-between;
        -webkit-box-align: center;
        align-items: center;
        margin: 24px 0px;
    }

    .css-1k0s06o {
        display: flex;
        -webkit-box-pack: start;
        justify-content: flex-start;
        -webkit-box-align: center;
        align-items: center;
        width: 100%;
    }

    .css-pyfdc7 {
        width: 170px;
        padding: 0px 0px 0px 16px;
    }

    .css-2qg9nh {
        font-size: 0.857143rem;
        line-height: 18px;
        color: var(--N700, rgba(49, 53, 59, 0.68));
    }

    .css-gcdhmf {
        font-weight: bold;
        font-size: 1rem;
        line-height: 20px;
        color: var(--N700, rgba(49, 53, 59, 0.96));
    }

    .css-f3ztd {
        padding: 0px 16px;
        border-right: 1px solid var(--N75, #E5E7E9);
        border-left: 1px solid var(--N75, #E5E7E9);
        width: 60%;
    }

    .css-2qg9nh {
        font-size: 0.857143rem;
        line-height: 18px;
        color: var(--N700, rgba(49, 53, 59, 0.68));
    }

    .css-gcdhmf {
        font-weight: bold;
        font-size: 1rem;
        line-height: 20px;
        color: var(--N700, rgba(49, 53, 59, 0.96));
    }

    .css-j4nz03 {
        width: 25%;
        padding-left: 16px;
    }

    .css-1bvc4cc {
        display: flex;
        -webkit-box-pack: end;
        justify-content: flex-end;
    }

    .css-18s3rjz {
        text-align: center;
        margin: 32px auto;
        width: 300px;
    }

    .css-1rahovo {
        width: 250px;
        height: 100%;
    }

    .css-w0kc19 {
        color: rgba(49, 53, 59, 0.68);
        font-size: 16px;
        font-weight: bold;
        line-height: 16px;
        margin: 1rem 0.25rem;
    }

    .css-1hnyl5u {
        color: rgba(49, 53, 59, 0.68);
        font-size: 14px;
        font-weight: unset;
        line-height: 16px;
        margin: 0.25rem 0.25rem 2rem;
    }

    .css-1k9qobw-unf-btn {
        color: rgb(255, 255, 255);
        font-size: 1rem;
        line-height: 18px;
        width: 100%;
        border-radius: 8px;
        font-weight: 700;
        outline: none;
        overflow: hidden;
        position: relative;
        text-overflow: ellipsis;
        transition: background 0.8s ease 0s;
        white-space: nowrap;
        display: block;
        background: var(--primary-color);
        border: none;
        text-indent: initial;
        padding: 10px 0;
        transition: all .4s ease-in-out;
    }

    .css-1k9qobw-unf-btn:hover {
        transition: all .4s ease-in-out;
        color: rgb(255, 255, 255);
        background-color: var(--primary-color);
        opacity: .9;
    }

    .badge-primary {
        background-color: var(--primary-color);
    }

    .DjhkItMLcf {
            display: flex;
            flex-direction: column;
            width: 100%;
        }

        .DjhkItMLcf .DQXPDCmiQw {
            background: #fff;
            border: 1px solid rgb(241, 241, 241);
            border-radius: 5px;
            padding: 12px;
            transition: 0.4s ease-out;
        }

        .DjhkItMLcf .DQXPDCmiQw:hover,
        .DjhkItMLcf .DQXPDCmiQw:has(input:checked) {
            transition: 0.4s ease-out;
            cursor: pointer;
            border-color: rgb(0, 174, 239);
            box-shadow: 0 0 rgba(0, 0, 0, 0), 0 0 rgba(0, 0, 0, 0), 0 0 rgba(0, 0, 0, 0), 0 0 rgba(0, 0, 0, 0), 0 10px 20px -5px rgba(0, 174, 239, 0.2), 0 5px 10px -5px rgba(0, 174, 239, 0.5);
        }

        .DjhkItMLcf .DQXPDCmiQw:has(input:checked) .pAEwcwpIAP .LFNFvicbva .vhrStrXljR {
            transition: 0.4s ease-out;
            width: 20px;
            height: 20px;
            background-color: rgb(0, 133, 255);
            border-radius: 50%;
            position: relative;
        }

        .DjhkItMLcf .DQXPDCmiQw:has(input:checked) .pAEwcwpIAP .LFNFvicbva .vhrStrXljR::after {
            content: "";
            width: 8px;
            height: 8px;
            border-radius: 50%;
            background-color: #fff;
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

        .DjhkItMLcf .DQXPDCmiQw .pAEwcwpIAP {
            display: flex;
            width: 100%;
            justify-content: space-between;
        }

        .DjhkItMLcf .DQXPDCmiQw .pAEwcwpIAP .LFNFvicbva {
            display: flex;
            gap: 10px;
        }

        .DjhkItMLcf .DQXPDCmiQw .pAEwcwpIAP .LFNFvicbva .vhrStrXljR {
            transition: 0.4s ease-out;
            width: 20px;
            height: 20px;
            background-color: #fff;
            border: 1px solid rgb(210, 210, 210);
            border-radius: 50%;
        }

        .DjhkItMLcf .DQXPDCmiQw .pAEwcwpIAP .LFNFvicbva .jppTUaEgYy {
            display: flex;
            gap: 8px;
            flex-direction: column;
        }

        .DjhkItMLcf .DQXPDCmiQw .pAEwcwpIAP .LFNFvicbva .jppTUaEgYy .zqYgwSlvmX {
            font-size: 14.4px;
            font-weight: 500;
            color: rgb(0, 0, 0);
            margin: 0;
        }

        .DjhkItMLcf .DQXPDCmiQw .pAEwcwpIAP .LFNFvicbva .jppTUaEgYy .dzCzoQWOaW {
            font-size: 12.4px;
            font-weight: 400;
            color: rgb(120, 120, 120);
            margin: 0;
            text-align: end;
            gap: 8px;
            flex-direction: row;
            display: flex;
            align-items: center;
        }

        .DjhkItMLcf .DQXPDCmiQw .pAEwcwpIAP .LFNFvicbva .jppTUaEgYy .dICQuslKpk {
            font-size: 13.4px;
            font-weight: 500;
            color: rgb(0, 84, 153);
            margin: 0;
        }

        .DjhkItMLcf .DQXPDCmiQw .pAEwcwpIAP .fHmhOeWFeI {
            display: flex;
            gap: 8px;
            flex-direction: column;
        }

        .DjhkItMLcf .DQXPDCmiQw .pAEwcwpIAP .fHmhOeWFeI .PzEWCBiGkx {
            font-size: 14.4px;
            font-weight: 500;
            color: rgb(0, 0, 0);
            margin: 0;
            text-align: end;
        }

        .DjhkItMLcf .DQXPDCmiQw .pAEwcwpIAP .fHmhOeWFeI .dzCzoQWOaW {
            font-size: 12.4px;
            font-weight: 400;
            color: rgb(120, 120, 120);
            margin: 0;
            text-align: end;
            gap: 8px;
            flex-direction: row;
            display: flex;
            align-items: center;
        }

        .DjhkItMLcf .DQXPDCmiQw .pAEwcwpIAP .fHmhOeWFeI .dzCzoQWOaW .TGCUZTQpRx {
            height: 90%;
            width: 1px;
            background: rgb(213, 213, 213);
        }

        .DjhkItMLcf .DQXPDCmiQw .line-hr {
            margin-top: 0.9rem !important;
            margin-bottom: 0.5rem !important;
            height: 1px;
            width: 100%;
            background: rgb(239, 240, 242);
        }

        .DjhkItMLcf .DQXPDCmiQw .relqpLAcxs {
            display: flex;
            padding-left: 30px;
        }

        .DjhkItMLcf .DQXPDCmiQw .relqpLAcxs.oUYXsUylHm {
            padding-left: 10px;
        }

        .DjhkItMLcf .DQXPDCmiQw .relqpLAcxs.oUYXsUylHm .URAXAoOcDM .AGZeUBWgnP {
            gap: 12px;
        }

        .DjhkItMLcf .DQXPDCmiQw .relqpLAcxs .URAXAoOcDM {
            list-style: none;
            padding-left: 0;
            margin: 0;
        }

        .DjhkItMLcf .DQXPDCmiQw .relqpLAcxs .URAXAoOcDM .AGZeUBWgnP {
            display: flex;
            gap: 10px;
            flex-direction: row;
            width: 100%;
            margin: 11px 0;
            align-items: center;
        }

        .DjhkItMLcf .DQXPDCmiQw .relqpLAcxs .URAXAoOcDM .AGZeUBWgnP .dec-icons {
            padding: 0.2rem;
            --tw-bg-opacity: 0.2;
            background-color: rgb(0 174 239/var(--tw-bg-opacity));
            border-radius: 9999px;
            align-items: center;
            display: flex;
            font-size: 10px;
            justify-content: center;
        }

        .DjhkItMLcf .DQXPDCmiQw .relqpLAcxs .URAXAoOcDM .AGZeUBWgnP .dec-icons svg {
            --tw-text-opacity: 1;
            color: rgb(0 158 237/var(--tw-text-opacity));
        }

        .DjhkItMLcf .DQXPDCmiQw .relqpLAcxs .URAXAoOcDM .AGZeUBWgnP .ciqANrnvYp {
            font-size: 15.4px;
        }

        .DjhkItMLcf .DQXPDCmiQw .haHukqnIXN {
            display: none;
        }

    @media screen and (max-width:767px) {
        .css-kqc12l {
            margin: 24px 0px 0px 0;
        }
    }

    @media screen and (max-width:847px) {
        .css-1hvhwyw {
            flex-direction: column;
            align-items: flex-start;
            gap: 10px;
        }

        .css-f3ztd {
            border: none;
            text-align: right;
        }

        .css-pyfdc7 {
            width: 50%;
        }

        .css-j4nz03 {
            width: 100%;
            padding-left: 70px;
        }
    }

    @media screen and (max-width:615px) {
        .css-1hvhwyw {
            flex-direction: column;
            align-items: flex-start;
            gap: 10px;
        }

        .css-f3ztd {
            border: none;
            text-align: right;
        }

        .css-pyfdc7 {
            width: 50%;
        }

        .css-j4nz03 {
            width: 100%;
            padding-left: 70px;
        }
    }

    @media screen and (max-width:447px) {
        .css-1k0s06o {
            flex-wrap: wrap;
        }

        .css-f3ztd {
            width: 100%;
            padding-left: 70px;
            margin-top: 13px;
            text-align: left;
        }

        .css-69i1ev {
            display: flex;
            gap: 10px;
            -webkit-box-pack: justify;
            justify-content: space-between;
            -webkit-box-align: center;
            align-items: flex-start;
            flex-direction: column;
        }
    }
</style>
